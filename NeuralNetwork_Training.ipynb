{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "445303af",
   "metadata": {},
   "source": [
    "# Defining and Training the Deep Learning Surrogate Models"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9b234cb3",
   "metadata": {},
   "source": [
    "For the paper, we left multiple hyperparameters variable and optimized them using Google Cloud Compute and Keras Tuner. Below is code to define and train the models described in the paper. The trained model checkpoints are also provided in the repository.\n",
    "\n",
    "Table of Contents:\n",
    "* [Step 1: Import Needed Python Packages](#step1)\n",
    "* [Step 2: Import and renormalize Data](#step2)\n",
    "* [Step 3: Define Model and Optimize Hyperparameters](#step3)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f9f7870c",
   "metadata": {},
   "source": [
    "### Step1: Import Python Packages <a class=\"anchor\" id=\"step1\"></a>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "cfaa890b",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "from tensorflow import keras\n",
    "from sklearn.model_selection import train_test_split\n",
    "from keras_tuner import BayesianOptimization\n",
    "import keras_tuner\n",
    "import datetime\n",
    "import pandas as pd\n",
    "import tqdm"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3a6fc412",
   "metadata": {},
   "source": [
    "### Step 2: Import Data and Renormalize to [0,1] <a class=\"anchor\" id=\"step2\"></a>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "b1e3180d",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Dataset imported, size:(508199, 116)\n"
     ]
    }
   ],
   "source": [
    "# Load Data\n",
    "data_URL = \"../../liquid-jet-data/fulldata-inputoutput-H.csv\"\n",
    "\n",
    "# important: Check whether the index is imported as one column as well.\n",
    "# If an index column is imported, set lower boundary of usecols to 1.\n",
    "\n",
    "data = pd.read_csv(data_URL, usecols=np.arange(1, 117), index_col=False)\n",
    "\n",
    "# # take only p polarized light:\n",
    "# data = data[data['3.0']==1.]\n",
    "# # drop the polarization column\n",
    "# data = data.drop(['3.0'], axis=1)\n",
    "\n",
    "\n",
    "# Convert data to a numpy array\n",
    "data_arr = data.to_numpy()\n",
    "\n",
    "# select only data with a0 > 1.\n",
    "a0_check = True\n",
    "\n",
    "if a0_check:\n",
    "    # a0 is equivalent to Pi_4e, at column position 13; index starts at 0 (numpy)\n",
    "    # (a0 should be greater 1 by construction of the simulation parameters anyway)\n",
    "    data_arr = data_arr[data_arr[:, 11] > 1.0]\n",
    "\n",
    "print(\"Dataset imported, size:\" + str(data_arr.shape))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1dfb4281",
   "metadata": {},
   "source": [
    "We want to imagine the neural network as being a function $f$ such that for $\\mathbf{x}= (E, \\{\\text{physical parameters}\\})$, we get $f(\\mathbf{x}) = \\text{d}n/\\text{d}E$. For that, we need to re-arrange our array."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "9de28928",
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "100%|██████████████████████████████████████████████| 508199/508199 [01:04<00:00, 7919.74it/s]\n"
     ]
    }
   ],
   "source": [
    "x = np.zeros((100 * data_arr.shape[0], 9))\n",
    "\n",
    "for i in tqdm.tqdm(range(data_arr.shape[0])):\n",
    "    for j in range(100):\n",
    "        x[int(100 * i + j)] = np.concatenate(\n",
    "            (np.array([data_arr[i, -1] * j / 100]), data_arr[i, :8])\n",
    "        )"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a7d6b165",
   "metadata": {},
   "source": [
    "The data is pretty big now as we can see:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "5dc50501",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(50819900, 9)"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "x.shape"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5b964f84",
   "metadata": {},
   "source": [
    "We still have to normalize it. We can do that by using the maximum values we defined in the sample space."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "58122026",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Oxygen charge\n",
    "x[:, 1] = x[:, 1] / 8.0  # maximum ionization of oxygen\n",
    "\n",
    "# Mixture\n",
    "x[:, 2] = (\n",
    "    x[:, 2] / 0.9\n",
    ")  # 90% is always the maximum of one particle species under investigation.\n",
    "\n",
    "# Energy\n",
    "x[:, 3] = x[:, 3] / 50\n",
    "\n",
    "# Focus-FWHM\n",
    "x[:, 4] = x[:, 4] / 20e-6\n",
    "\n",
    "# Pulse duration\n",
    "x[:, 5] = x[:, 5] / 150e-15\n",
    "\n",
    "# Incidence angle\n",
    "x[:, 6] = x[:, 6] / 85\n",
    "\n",
    "# Wavelength\n",
    "x[:, 7] = x[:, 7] / 1100e-9\n",
    "\n",
    "# Target thickness\n",
    "x[:, 8] = x[:, 8] / 3e-6"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "a2a5c8a0",
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "100%|█████████████████████████████████████████████| 508199/508199 [00:16<00:00, 30897.92it/s]\n"
     ]
    }
   ],
   "source": [
    "# preparing y similarly\n",
    "y = np.zeros((100 * data_arr.shape[0], 1))\n",
    "\n",
    "for i in tqdm.tqdm(range(data_arr.shape[0])):\n",
    "    for j in range(15, 115):\n",
    "        y[int(100 * i + j - 15)] = data_arr[i, j]\n",
    "\n",
    "# forcing log(0) = 0\n",
    "x[x == -np.inf] = 0\n",
    "y[y == -np.inf] = 0"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1f45a15f",
   "metadata": {},
   "source": [
    "(if we wanted to train the Emax model instead, we would not need to rearrange the arrays as much:)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "acaeb1b0",
   "metadata": {},
   "outputs": [],
   "source": [
    "# x = dataT[:, :9]\n",
    "# y = dataT[:, -1] / 2\n",
    "\n",
    "# # for deuterons the y data has to normalized as well.\n",
    "# maxY = np.min(y.flatten())\n",
    "# y = y / maxY\n",
    "# y = np.log10(y)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "id": "dc18a666",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Available data: 50819900 data points. (41164119.0 training, 4573791.0 validation, 5081990 testing)\n"
     ]
    }
   ],
   "source": [
    "# Split Train, Test (Validation split is done later during training)\n",
    "x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.1)\n",
    "\n",
    "# (we will later make 10% validation data again)\n",
    "print(\n",
    "    \"Available data: \"\n",
    "    + str(len(x))\n",
    "    + \" data points. (\"\n",
    "    + str(0.9 * len(x_train))\n",
    "    + \" training, \"\n",
    "    + str(0.1 * len(x_train))\n",
    "    + \" validation, \"\n",
    "    + str(len(x_test))\n",
    "    + \" testing)\"\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "449ace53",
   "metadata": {},
   "source": [
    "### Step 3: Model Definition and Hyperparameter Optimization <a class=\"anchor\" id=\"step3\"></a>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "08fd4805",
   "metadata": {},
   "source": [
    "Here, we will define a fully-connected network model which finds its best architecture and regularization using Keras Tuner. The architecture is optimized first, and then the regularization is done in the same way by using the previously determined architecture."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "id": "4031966e",
   "metadata": {},
   "outputs": [],
   "source": [
    "l1 = keras.regularizers.l1(l1=1e-9)\n",
    "l2 = keras.regularizers.l2(l2=5e-9)\n",
    "\n",
    "# # we can keep model checkpoints during training if we like by providing a desired output path\n",
    "# savedmodel = \"best_model_FullFunc_2.h5\"\n",
    "\n",
    "callbacks = [\n",
    "    # keras.callbacks.ModelCheckpoint(\n",
    "    #     savedmodel, save_best_only=True, monitor=\"val_loss\"\n",
    "    # ),\n",
    "    tf.keras.callbacks.TensorBoard(log_dir=\"./logs\"),\n",
    "    keras.callbacks.ReduceLROnPlateau(\n",
    "        monitor=\"val_loss\", factor=0.5, patience=2, min_lr=0.0001\n",
    "    ),\n",
    "    keras.callbacks.EarlyStopping(monitor=\"val_loss\", patience=3, verbose=1),\n",
    "]\n",
    "\n",
    "epochs = 1000\n",
    "batch_size = 256  # can be adapted based on availabe memory. You might want to set this much higher."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "36acd606",
   "metadata": {},
   "outputs": [],
   "source": [
    "HPS = keras_tuner.HyperParameters()\n",
    "\n",
    "# find best architecture\n",
    "HPS.Int(\"num_layers\", 5, 8, default=5)\n",
    "for i in range(8):\n",
    "    HPS.Int(\"units_\" + str(i), min_value=128, max_value=512, step=32)\n",
    "\n",
    "\n",
    "def build_model(hp):\n",
    "    input_layer = keras.layers.Input(x_train.shape[1])\n",
    "    layer = input_layer\n",
    "\n",
    "    for i in range(hp.get(\"num_layers\")):\n",
    "        layer = keras.layers.Dense(units=hp.get(\"units_\" + str(i)), activation=\"relu\")(\n",
    "            layer\n",
    "        )\n",
    "\n",
    "    output_layer = keras.layers.Dense(y_train.shape[1])(layer)\n",
    "\n",
    "    model = keras.models.Model(inputs=input_layer, outputs=output_layer)\n",
    "    model.compile(\n",
    "        optimizer=\"adam\",\n",
    "        loss=keras.losses.MeanSquaredError(),\n",
    "        metrics=[keras.metrics.MeanSquaredError()],\n",
    "    )\n",
    "    model.summary()\n",
    "    return model\n",
    "\n",
    "\n",
    "distribution_strategy = tf.distribute.MirroredStrategy()\n",
    "\n",
    "tuner = BayesianOptimization(\n",
    "    build_model,\n",
    "    objective=\"val_mean_squared_error\",\n",
    "    hyperparameters=HPS,\n",
    "    max_trials=100,\n",
    "    executions_per_trial=10,\n",
    "    overwrite=False,\n",
    "    directory=\".\",\n",
    "    project_name=\"IonOpti_2\",\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f8ac04d4",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "tuner.search_space_summary()\n",
    "tuner.search(\n",
    "    x_train,\n",
    "    y_train,\n",
    "    batch_size=batch_size,\n",
    "    epochs=epochs,\n",
    "    callbacks=callbacks,\n",
    "    validation_split=0.1,  # here's the 10% validation mentioned earlier\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2d3f1741",
   "metadata": {},
   "outputs": [],
   "source": [
    "tuner.results_summary(1)\n",
    "best_model = tuner.get_best_models(1)[0]\n",
    "best_hyperparameters = tuner.get_best_hyperparameters(1)[0]\n",
    "\n",
    "# References to best trial assets\n",
    "best_trial_id = tuner.oracle.get_best_trials(1)[0].trial_id\n",
    "best_trial_dir = tuner.get_trial_dir(best_trial_id)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "819e5330",
   "metadata": {},
   "source": [
    "We can now save our best model if we like"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8f92309a",
   "metadata": {},
   "outputs": [],
   "source": [
    "# best_model.save(\"best_model_dndE_GSCE.h5\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "02f3ca7d",
   "metadata": {},
   "source": [
    "To obtain the best model with regularization we re-define the `build_model` function with the architecture that we found from the previous hyperparameter search:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c228e0f6",
   "metadata": {},
   "outputs": [],
   "source": [
    "HPS.Float(\"l1_val\", min_value=1e-9, max_value=1.0, sampling=\"log\")\n",
    "HPS.Float(\"l2_val\", min_value=1e-9, max_value=1.0, sampling=\"log\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "id": "2a437835",
   "metadata": {},
   "outputs": [],
   "source": [
    "def build_model(hp):\n",
    "    input_layer = keras.layers.Input(x_train.shape[1])\n",
    "    layer = input_layer\n",
    "    layer = keras.layers.Dense(\n",
    "        units=320,\n",
    "        activation=\"relu\",\n",
    "        kernel_regularizer=keras.regularizers.l1_l2(\n",
    "            l1=hp.get(\"l1_val\"), l2=hp.get(\"l2_val\")\n",
    "        ),\n",
    "    )(layer)\n",
    "    layer = keras.layers.Dense(\n",
    "        units=288,\n",
    "        activation=\"relu\",\n",
    "        kernel_regularizer=keras.regularizers.l1_l2(\n",
    "            l1=hp.get(\"l1_val\"), l2=hp.get(\"l2_val\")\n",
    "        ),\n",
    "    )(layer)\n",
    "    layer = keras.layers.Dense(\n",
    "        units=288,\n",
    "        activation=\"relu\",\n",
    "        kernel_regularizer=keras.regularizers.l1_l2(\n",
    "            l1=hp.get(\"l1_val\"), l2=hp.get(\"l2_val\")\n",
    "        ),\n",
    "    )(layer)\n",
    "    layer = keras.layers.Dense(\n",
    "        units=256,\n",
    "        activation=\"relu\",\n",
    "        kernel_regularizer=keras.regularizers.l1_l2(\n",
    "            l1=hp.get(\"l1_val\"), l2=hp.get(\"l2_val\")\n",
    "        ),\n",
    "    )(layer)\n",
    "    layer = keras.layers.Dense(\n",
    "        units=256,\n",
    "        activation=\"relu\",\n",
    "        kernel_regularizer=keras.regularizers.l1_l2(\n",
    "            l1=hp.get(\"l1_val\"), l2=hp.get(\"l2_val\")\n",
    "        ),\n",
    "    )(layer)\n",
    "    layer = keras.layers.Dense(\n",
    "        units=320,\n",
    "        activation=\"relu\",\n",
    "        kernel_regularizer=keras.regularizers.l1_l2(\n",
    "            l1=hp.get(\"l1_val\"), l2=hp.get(\"l2_val\")\n",
    "        ),\n",
    "    )(layer)\n",
    "\n",
    "    output_layer = keras.layers.Dense(y_train.shape[1])(layer)\n",
    "\n",
    "    model = keras.models.Model(inputs=input_layer, outputs=output_layer)\n",
    "    model.compile(\n",
    "        optimizer=\"adam\",\n",
    "        loss=keras.losses.MeanSquaredError(),\n",
    "        metrics=[keras.metrics.MeanSquaredError()],\n",
    "    )\n",
    "    model.summary()\n",
    "    return model"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bbef3c22",
   "metadata": {},
   "source": [
    "Now we can search the same way as before"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6b6f97e1",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "tuner.search_space_summary()\n",
    "tuner.search(\n",
    "    x_train,\n",
    "    y_train,\n",
    "    batch_size=batch_size,\n",
    "    epochs=epochs,\n",
    "    callbacks=callbacks,\n",
    "    validation_split=0.1,\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "747d08f6",
   "metadata": {},
   "outputs": [],
   "source": [
    "tuner.results_summary(1)\n",
    "best_model = tuner.get_best_models(1)[0]\n",
    "best_hyperparameters = tuner.get_best_hyperparameters(1)[0]\n",
    "\n",
    "# References to best trial assets\n",
    "best_trial_id = tuner.oracle.get_best_trials(1)[0].trial_id\n",
    "best_trial_dir = tuner.get_trial_dir(best_trial_id)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3bbee497",
   "metadata": {},
   "source": [
    "Now we can save our new best model that is fully optimized:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7d1b41fb",
   "metadata": {},
   "outputs": [],
   "source": [
    "# best_model.save(\"best_model_dndE_GSCE.h5\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0dab377f",
   "metadata": {},
   "source": [
    "### Above we demonstrated the training of the continuous model. The training of the maximum energy model is done equivalently."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
