# Liquid Leaf Modeling Scripts

[![License: Apache 2.0](https://img.shields.io/badge/License-cc-Attribution4.svg)](https://git.rwth-aachen.de/surrogat-models/liquid-leaf-modeling-scripts/-/blob/main/LICENSE)
<!-- [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.6383868.svg)](https://doi.org/10.5281/zenodo.6383868) -->

## Table of contents
1. [Introduction](#introduction)
2. [Strucuture of Folder Tree](#folderstructure)
3. [Known Problems](#bugs)
4. [Publications & Links](#publications)
5. [Contributers](#contributors)
6. [Funding](#funding)

## 1. Introduction <a name="introduction"></a>

This repository cotains the network training scripts and resulting models for the liquid jet modeling paper [1]. 
The scripts utilize Keras [2] and tensorflow 2 [3].
It is part of the LOEWE NP research cluster at TU Darmstadt [4] and the work was done at the Fachgebiet Beschleunigerphysik at TU Darmstadt [5]

## 2. Structure of Folder Tree <a name="folderstructure"></a>

The files contain the following:
* `NeuralNetwork_Training.ipynb`: Jupyter Notebook for training the model.
* `NeuralNetwork_Application.ipynb`: Jupyter Notebook with example applications of the models. 
* `best_model_dndE_CloudTuner.h5`: Reduced data model's spectrum prediction.
* `best_tuned_model_Emax.h5`: Reduced data model's cut off prediction.
* `new_ideal_2.h5`: Full data model's spectrum prediction.
* `new_ideal_Emax.h5`: Full data model's cut off prediction.
* `vega_init_h.npy`: Numpy based control dataset, it includes the spectral data of a PIC simulation with the initial laser parameters, used in the paper. 

## 4. Known Problems <a name="bugs"></a>
The data for training has to be fetched from the corresponding repository to allow for training. ([gitlab](https://git.rwth-aachen.de/surrogat-models/liquid-jet-data)|[tudata]).


## 5. Publications & Links <a name="publications"></a>

[1] [Modeling of a Liquid Leaf Target TNSA Experiment Using Particle-In-Cell Simulations and Deep Learning](https://doi.org/10.1155/2023/2868112)

[2] [Keras](https://keras.io/)

[3] [tensorflow](https://www.tensorflow.org/)

[4] [LOEWE center for Nuclear Photonics](https://www.ikp.tu-darmstadt.de/nuclearphotonics/nuclear_photonics/index.en.jsp) 

[5] [Accelerator Pyhsics at TEMF, TU Darmstadt](https://www.bp.tu-darmstadt.de/fachgebiet_beschleunigerphysik/index.en.jsp)

## 6. Contributors <a name="contributors"></a>
Contributors include (alphabetically): 
*   D. Kreuter, 
*   B. Schmitz

## 7. Funding <a name="funding"></a>
* This work was funded by HMWK through the LOEWE center “Nuclear Photonics”.
* The training used computational ressources from the Graduate School CE within the Centre for Computational Engineering at Technische Universität Darmstadt.
